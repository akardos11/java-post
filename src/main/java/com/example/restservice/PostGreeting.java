package com.example.restservice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PostGreeting {

    private final String date;
    private final String content;
    private String pattern = "MM/dd/yyyy HH:mm:ss";
    private DateFormat df = new SimpleDateFormat(pattern);
    private Date today = Calendar.getInstance().getTime();
    private String todayAsString = df.format(today);

    public PostGreeting(String content) {
        this.date = todayAsString;
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }
}
